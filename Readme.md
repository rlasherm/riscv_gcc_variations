# Custom riscv toolchain

## Setting toolchain name

In *env/riscv-gcc-buildenv.nix*, change the **VENDOR** variable to name your toolchain, replacing "unknown" in the toolchain triplet.

## Scrispan v1

*fence.t* equivalent : just a barrier.


## How to generate custom toolchain

We are following the tutorial from [https://hsandid.github.io/posts/risc-v-custom-instruction/](https://hsandid.github.io/posts/risc-v-custom-instruction/).

Get the riscv-opcodes tool (using commit 0a858fa5caf20addafc9472ad3ec7489fe5f6b9b as of writting) :
```
git clone https://github.com/riscv/riscv-opcodes
```

Create the file *rv_scrispan_opcodes* (already done in this repo normally) with the instructions you want to add :

```
scrispan.fence    31..15=0 14..12=4 11..7=0 6..2=0x03 1..0=3
```

Copy this file at tho root of the *riscv-opcodes* folder.

Then create the masks, in the *riscv-opcodes* folder with :

```
python3 parse.py -c rv_scrispan_opcodes
```

We use MASK and MATCH values in the resulting *encoding.out.h* file and insert them inside *binutils/include/opcodes/riscv-opc.h*.
Add the instruction declaration in this same header file.

```
DECLARE_INSN(scrispan_fence, MATCH_SCRISPAN_FENCE, MASK_SCRISPAN_FENCE)
```

In *binutils/opcodes/riscv-opc.c*, add the instructions declaration.

```
{"scrispan.fence",  0, INSN_CLASS_I,   "",    MATCH_SCRISPAN_FENCE,  MASK_SCRISPAN_FENCE,  match_opcode, 0},
```

Then call *build.sh* to rebuild the toolchain

Finally, create the archive.

```bash
tar -cJf riscv-medany-customfence.tar.xz riscv64-scrispan1-elf
```