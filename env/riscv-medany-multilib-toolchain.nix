with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/riscv_gcc_nix/-/raw/master/riscv-medany.tar.xz?inline=false";
        hash =  "sha256-5T9ExLJvwG4vpNQRgjTcg7bq0T0b6yjhVgaEuh/KV0A=";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}

