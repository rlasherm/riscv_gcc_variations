with import <nixpkgs> {};

let
     pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify                
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-22.05";                     
        rev = "ce6aa13369b667ac2542593170993504932eb836";                                           
    }) {};

in

stdenv.mkDerivation {
    hardeningDisable = [ "format" ];
    name = "riscv-gcc";
    RISCV = toString ../riscv;
    VENDOR = "inria";

    nativeBuildInputs = [
        pkgs.git
        pkgs.gcc
        pkgs.curl
        pkgs.perl
        pkgs.wget
        pkgs.bison
        pkgs.flex
        pkgs.texinfo
        pkgs.expat
        pkgs.automake
        pkgs.autoconf
        pkgs.python3
        pkgs.libmpc
        pkgs.gawk
        pkgs.libtool
        pkgs.patchutils
        pkgs.gperf
        pkgs.bc
        pkgs.gmp
        pkgs.mpfr
        pkgs.isl
        pkgs.openssl
        pkgs.cacert
    ];

    shellHook = ''
        mkdir -p $RISCV
        export PATH=$PATH:$RISCV/bin
    '';
}
