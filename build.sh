#!/usr/bin/env nix-shell
#!nix-shell env/riscv-gcc-buildenv.nix -i bash

make clean
./configure --prefix=$RISCV --enable-multilib --with-cmodel=medany
make -j4

